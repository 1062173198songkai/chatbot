import asyncio
import websockets
import json  # 假设消息是JSON格式


async def hello():
    uri = "ws://localhost:6789"
    async with websockets.connect(uri) as websocket:
        print("Connected to the server")
        await websocket.send("Hello, server!")  # 发送一个消息到服务器

        try:
            while True:  # 持续监听消息
                message = await websocket.recv()
                print(f"Received raw message: {message}")

                # 解析JSON消息
                try:
                    data = json.loads(message)
                    # 假设消息结构包含 'sequence', 'has_next', 和 'content'
                    sequence = data.get('sequence', -1)
                    has_next = data.get('has_next', False)
                    content = data.get('message', '')
                    print(f"Sequence: {sequence}, Message: {content}, Has next: {has_next}")

                    # 如果没有下一个消息，则可选择退出循环
                    if not has_next:
                        print("Received the final message. Closing connection.")
                        break
                except json.JSONDecodeError:
                    print("Received non-JSON message.")

        except websockets.exceptions.ConnectionClosed:
            print("Connection closed by the server.")
        except Exception as e:
            print(f"Error occurred: {e}")
        finally:
            await websocket.close()
            print("Connection closed.")


asyncio.run(hello())
