import threading
import queue


class FairLock:
    def __init__(self):
        self.queue = queue.Queue()
        self.lock = threading.Lock()

    def __enter__(self):
        q = queue.Queue()
        self.queue.put(q)

        with self.lock:
            if self.queue.queue[0] != q:
                q.get()  # 等待直到这个队列是第一个队列

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        with self.lock:
            self.queue.get()  # 移除队列中的第一个元素
            if not self.queue.empty():
                self.queue.queue[0].put(None)  # 唤醒下一个等待的线程