import base64
import io
import json
import os
import threading
import tkinter
from tkinter import *
from tkinter import filedialog
from tkinter import scrolledtext
from typing import Dict, List

import customtkinter
import pyaudio
from PIL import Image, ImageTk
from colorama import Fore

import freegpt
import gpt
import recognize
import voice
from FairLock import FairLock
from my_websocket import WebSocketClient
from vosk import Model, KaldiRecognizer

# Vosk模型路径，请根据实际情况进行修改
model_path = "vosk/en"

# 初始化Vosk模型
model = Model(model_path)
rec = KaldiRecognizer(model, 16000)

# 录音的参数
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
CHUNK = 1024

# 录音状态
is_recording = False

# 初始化PyAudio
p = pyaudio.PyAudio()


# 定义开始录音并实时识别的函数
def start_recording():
    print("start_recording...")

    global is_recording
    is_recording = True

    stream = p.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True, frames_per_buffer=CHUNK)
    print("recording...")

    while is_recording:
        data = stream.read(CHUNK, exception_on_overflow=False)
        if rec.AcceptWaveform(data):
            result = json.loads(rec.Result())
            print(result.get('text', ''))

    print("finished recording")

    # 停止并关闭流
    stream.stop_stream()
    stream.close()


# 定义停止录音的函数
def stop_recording():
    global is_recording
    is_recording = False


# pip install -U g4f
apis = open('keys.apikey').read().split(', ')

root = customtkinter.CTk()  # create CTk window like you do with the Tk window
root.title("ChatBot")
customtkinter.set_appearance_mode(
    "dark")  # Modes: system (default), light, dark  # Themes: blue (default), dark-blue, green

root.geometry()
ALL_OPS_BUTTONS: List[customtkinter.CTkButton] = []
stability = PhotoImage(file="img/stability.png")
main_image = Image.open("img/image.png")
mega_image = PhotoImage(file="img/logo.png")
tem_image = Image.open("img/logo.png")

mega_ctk_image = customtkinter.CTkImage(tem_image)
width_photo, height_photo = main_image.size
main_image = main_image.resize((int(width_photo // 4.2), int(height_photo // 4.2
                                                             )))  ## The (250, 250) is (height, width)

gitlogo = PhotoImage(file="img/logo.png")
main_image.save('img/temp.png')
main_image = PhotoImage(file="img/temp.png")

# qrcode=PhotoImage(file="img/qr-code.png")
about = """ChatBooooooot \r\n Activating the 'enable speaking' switch enables automatic vocalization of the text 
content returned by the chatbox."""

#
# Use CTkButton instead of tkinter Button
# button = customtkinter.CTkButton(master=app, text="CTkButton", command=button_function)
# button.place(relx=0.5, rely=0.5, anchor=customtkinter.CENTER)

active_voice = BooleanVar(value=False)

models = ['stable-diffusion-v1',
          'stable-diffusion-v1-5',
          'stable-diffusion-512-v2-0',
          'stable-diffusion-768-v2-0',
          'stable-diffusion-512-v2-1',
          'stable-diffusion-768-v2-1',
          'stable-inpainting-v1-0',
          'stable-inpainting-512-v2-0',
          'esrgan-v1-x2plus',
          'stable-diffusion-xl-beta-v2-2-2',
          'stable-diffusion-xl-1024-v0-9',
          'stable-diffusion-xl-1024-v1-0',
          'stable-diffusion-x4-latent-upscaler',
          ]

samplers_list = ['DDIM',
                 'PLMS',
                 'K_EULER',
                 'K_EULER_ANCESTRAL',
                 'K_HEUN',
                 'K_DPM_2',
                 'K_DPM_2_ANCESTRAL',
                 'K_DPMPP_2S_ANCESTRAL',
                 'K_DPMPP_2M',
                 'K_DPMPP_SDE']

sizes = [
    '1024x1024',
    '1152x896',
    '896x1152',
    '1216x832',
    '832x1216',
    '1344x768',
    '768x1344',
    '1536x640',
    '640x1536',
    '512x512'
]

gptmodel = ['gpt-3.5-turbo',
            'gpt-4',
            'gpt-4-turbo']

'''['palm',
'h2ogpt-gm-oasst1-en-2048-falcon-7b-v3',
'h2ogpt-gm-oasst1-en-2048-falcon-40b-v1',
'h2ogpt-gm-oasst1-en-2048-open-llama-13b',
'claude-instant-v1',
'claude-v1',
'claude-v2',
'command-light-nightly',
'command-nightly',
'gpt-neox-20b',
'oasst-sft-1-pythia-12b',
'oasst-sft-4-pythia-12b-epoch-3.5',
'santacoder',
'bloom',
'flan-t5-xxl',
'code-davinci-002',
'text-ada-001',
'text-babbage-001',
'text-curie-001',
'text-davinci-002',
'text-davinci-003',
'llama13b-v2-chat',
'llama7b-v2-chat']
'''
customtkinter.set_default_color_theme("green")  # Themes: "blue" (standard), "green", "dark-blue"
root.geometry()
root.option_add("*tearOff", FALSE)
root.resizable(True, True)
count_message = -1
voices = ['en_0', 'en_1', 'en_2', 'en_3', 'en_4', 'en_5', 'random']
voice_var = StringVar(value=voices[1])
temp_var = DoubleVar(value=0.7)

free = BooleanVar(value=False)


def listen():
    global count_message
    count_message += 3
    slyx = recognize.recognize_speak()
    print(Fore.BLUE + 'You: ', slyx)
    print(Fore.WHITE)
    st.insert((str(count_message) + '.0'), os.getlogin() + ': ' + slyx + '\n\n')
    count_message += 3
    user_answer_entry.delete("0", "end")
    if free.get():
        ans = freegpt.freegpt(str(slyx), combobox_models.get(), temp_var.get())
    else:
        ans = gpt.answer(str(slyx), temp_var.get())
    st.insert((str(count_message) + '.0'), 'CustomGPT: ' + ans + '\n\n')
    print(Fore.GREEN + 'CustomGPT: ', ans)
    if active_voice.get() == True:
        voice.speak(ans, speaker=combobox.get())


message_handler_lock: FairLock = FairLock()
# 是否允许发送
is_allowed_send: bool = True


def message_handler(message: str):
    global is_allowed_send

    with message_handler_lock:
        try:
            data: Dict = json.loads(message)
            print(f"type of data:{type(data)}")
            sequence = data.get('sequence', -1)
            has_next = data.get('has_more', False)
            content = data.get('message', '')
            message_type: str = data.get("type", "text")
            print(f"Sequence: {sequence}, Message: {content}, Has next: {has_next}")
            # st.config(state=tk.NORMAL)
            if sequence == 0:
                st.insert("end", '\n')
                st.insert("end", 'ChatBot: ')
            if message_type == 'text':
                st.insert("end", content.strip())
                print(f"has_next:{has_next}")
                if active_voice.get():
                    voice.speak(content, speaker=combobox.get())
            elif message_type == 'image':
                st.insert("end", '\n')
                cur_image_data = base64.b64decode(content)
                cur_image = Image.open(io.BytesIO(cur_image_data))
                cur_image = cur_image.resize((256, int(cur_image.height * (256 / cur_image.width))))
                global cur_tk_image  # 使图片对象保持活跃
                cur_tk_image = ImageTk.PhotoImage(cur_image)
                st.image_create(tkinter.END, image=cur_tk_image)
            if has_next:
                is_allowed_send = False
            else:
                _unlock_all_button()
                is_allowed_send = True

        except json.JSONDecodeError:
            print("Received non-JSON message.")
            _unlock_all_button()
            is_allowed_send = True
        # finally:
        # st.config(state=tk.DISABLED)


client = WebSocketClient(uri="ws://localhost:6789", message_handler=message_handler)
client.connect()


def typing(event=None):
    global count_message
    ans = ''
    count_message += 3
    print(Fore.BLUE + 'You: ', user_answer_entry.get())
    print(Fore.WHITE)
    st.insert("end", '\n')
    st.insert("end", "You" + ': ' + user_answer_entry.get())
    count_message += 3
    content = str(user_answer_entry.get())
    if not content or len(content.strip()) == 0:
        _unlock_all_button()
        return
    _lock_all_button()
    client.send_message(json.dumps({
        "type": "text",
        "content": str(user_answer_entry.get())
    }, ensure_ascii=False))
    # client.send_message(str(user_answer_entry.get()))
    # client.register_message_handler(message_handler)
    # if free.get():
    #     ans = freegpt.freegpt(str(user_answer_entry.get()), combobox_models.get(), temp_var.get())
    # else:
    #     ans = gpt.answer(str(user_answer_entry.get()), temp_var.get())
    # user_answer_entry.delete("0", "end")
    # st.insert((str(count_message) + '.0'), 'CustomGPT: ' + ans + '\n\n')
    # print(Fore.GREEN + 'CustomGPT: ', ans)
    # if active_voice.get():
    #     voice.speak(ans, speaker=combobox.get())
    # user_answer_entry.delete(0, tkinter.END)


def update_slider(*args):
    temp_var.set(round(temp_var.get(), 2))


def choise_dir():
    global pathimg
    pathimg = filedialog.askdirectory()
    save_btn.configure(fg_color="green")


def generate():
    global count_message
    _lock_all_button()
    ans = ''
    count_message += 3
    print(Fore.BLUE + 'You: ', user_answer_entry.get())
    print(Fore.WHITE)
    st.insert("end", '\n')
    st.insert("end", 'You: ' + ': ' + user_answer_entry.get())
    count_message += 3
    client.send_message(json.dumps({
        "type": "image",
        "content": str(user_answer_entry.get())
    }, ensure_ascii=False))
    # global main_image
    # (width, height) = (width_entry.get()).split('x')
    # # text_to_image.texttoimage(api=apis[2],
    # #             prompt=user_answer_entry.get(),
    # #             engine=model_combo.get(),
    # #             width=int(width),
    # #             height=int(height),
    # #             sampler=exec('generation.SAMPLER_' + str(sampler_combo.get())),
    # #             path=pathimg)
    #
    # main_image = Image.open(f"{pathimg}/{user_answer_entry.get()}.png")
    #
    # width_photo, height_photo = main_image.size
    # main_image = main_image.resize((width_photo // 3, height_photo // 3))  ## The (250, 250) is (height, width)
    # main_image.save('img/tempimg.png')
    # main_image = PhotoImage(file="img/tempimg.png")
    # image_mega.configure(image=main_image)


def _lock_all_button():
    for btn in ALL_OPS_BUTTONS:
        btn.configure(state=DISABLED)


def _unlock_all_button():
    for btn in ALL_OPS_BUTTONS:
        btn.configure(state=NORMAL)


st = scrolledtext.ScrolledText(root, wrap='word', width=80, height=30, highlightbackground="#252425",
                               highlightcolor="#252425", highlightthickness=2)
st.grid(column=0, row=0, columnspan=40, rowspan=30)
st.config(bg='#252425', fg='white')
st.bind("<Return>", typing)
st.focus_set()

# st.config(state=tk.DISABLED)

base64_image = """
iVBORw0KGgoAAAANSUhEUgAAAAUA
AAAFCAYAAACNbyblAAAAHElEQVQI12P4
//8/w38GIAXDIBKE0DHxgljNBAAO
9TXL0Y4OHwAAAABJRU5ErkJggg==
"""

# 解码 base64 字符串
test_image_data = base64.b64decode(base64_image)

# 使用 PIL 来打开图像对象
test_image = Image.open(io.BytesIO(test_image_data))
tk_image = ImageTk.PhotoImage(test_image)

# st.image_create(tkinter.END, image=tk_image)


# user_answer_entry = customtkinter.CTkEntry(root, width=600, placeholder_text="请输入 ")
# user_answer_entry.grid(row=31, column=0, columnspan=40)
# user_answer_entry.bind("<Return>", typing)


# 创建一个CTkFrame作为CTkEntry和图标按钮的容器
entry_frame = customtkinter.CTkFrame(master=root, width=620, height=40, corner_radius=10)
entry_frame.grid(row=31, column=0, columnspan=40)
entry_frame.grid_propagate(False)  # 防止内部控件改变Frame大小

# 在容器内创建CTkEntry
user_answer_entry = customtkinter.CTkEntry(master=entry_frame, width=600, height=36, corner_radius=10,
                                           placeholder_text="请输入 ")
user_answer_entry.place(x=10, y=2)  # 使用绝对布局来精确放置

icon_image = Image.open("img/upload.png")

icon_image = customtkinter.CTkImage(icon_image)
# 在容器内创建CTkButton作为图标按钮
icon_button = customtkinter.CTkButton(master=entry_frame, text="", command=typing, width=20, height=20, corner_radius=5,
                                      image=icon_image)
icon_button.place(x=570, y=6)  # 调整位置使其显示在CTkEntry的右侧

icon_button.icon_image = icon_image  # 保存对图像的引用
icon_button.configure(image=icon_button.icon_image)
ALL_OPS_BUTTONS.append(icon_button)

user_answer_button = customtkinter.CTkButton(root, text="Send", width=262, command=typing)
user_answer_button.grid(row=32, column=0, columnspan=18)
ALL_OPS_BUTTONS.append(user_answer_button)

entry_frame2 = tkinter.Frame(master=root, width=100, height=40)
entry_frame2.grid(row=32, column=19, columnspan=10)
entry_frame2.grid_propagate(False)  # 防止内部控件改变Frame大小

# 在容器内创建Button作为录音按钮
mic_btn = tkinter.Button(entry_frame2, text="Voice", width=100, height=40, background='#54a276',
                         activebackground='#306846',
                         font='black', foreground='#54a276', command=lambda: print("Mic Button Clicked"))
mic_btn.place(x=0, y=0)  # 调整位置使其显示在容器的右侧

# 绑定鼠标按下和释放事件到录音函数
mic_btn.bind("<ButtonPress>", lambda e: threading.Thread(target=start_recording, daemon=True).start())
mic_btn.bind("<ButtonRelease>", lambda e: stop_recording())

# mic_btn = customtkinter.CTkButton(root, text="voice", width=150)
# mic_btn.grid(row=32, column=19, columnspan=10)
# ALL_OPS_BUTTONS.append(mic_btn)
#
# # 绑定鼠标按下和释放事件
# mic_btn.bind("<ButtonPress>", lambda e: threading.Thread(target=start_recording, daemon=True).start())
# mic_btn.bind("<ButtonRelease>", lambda e: stop_recording())

img_btn = customtkinter.CTkButton(root, text="image generation", width=150, command=generate)
img_btn.grid(row=32, column=29, columnspan=12)
ALL_OPS_BUTTONS.append(img_btn)

image_label = customtkinter.CTkLabel(root, image=mega_image, text="")
image_label.grid(row=0, column=45, columnspan=90, rowspan=1)
_unlock_all_button()
about_text = customtkinter.CTkLabel(root, text=about, justify="left", text_color='#888888')
about_text.grid(row=1, column=45, columnspan=20, rowspan=12)

settings_text = customtkinter.CTkLabel(root, text="", justify="center", text_color='#DDDDDD',
                                       font=('Arial', 20))
settings_text.grid(row=13, column=45, columnspan=5, rowspan=5)

label = customtkinter.CTkLabel(root, text="voice: ")
label.grid(row=18, column=45, columnspan=3)

combobox = customtkinter.CTkComboBox(root, values=voices, width=320)
combobox.grid(row=18, column=50, columnspan=20)

# temp = customtkinter.CTkLabel(root, text="temperature: ")
# temp.grid(row=20, column=45, columnspan=5)

# slider = customtkinter.CTkSlider(root, from_=0, to=2, width=170, variable=temp_var, command=update_slider)
# slider.grid(row=20, column=50, columnspan=10)

# btn_temp = customtkinter.CTkButton(root, textvariable=temp_var)
# btn_temp.grid(row=20, column=60, columnspan=20)

voice_active = customtkinter.CTkSwitch(root, text="enable speaking", variable=active_voice)
voice_active.grid(row=22, column=45, columnspan=8)

label = customtkinter.CTkLabel(root, text="mode selection")
label.grid(row=31, column=45, columnspan=6)

combobox_models = customtkinter.CTkComboBox(root, values=gptmodel, width=200)
combobox_models.grid(row=31, column=56, columnspan=20)

# gitbutton=customtkinter.CTkButton(root, width=240, fg_color="black", text_color="white", text="Проект на GitHub")
# gitbutton.grid(row=22, column=45, columnspan=6, rowspan=30)

# qrcodelabel=customtkinter.CTkLabel(root, text="", image=qrcode)
# qrcodelabel.grid(row=22, column=55, columnspan=30, rowspan=30)

####################################STABILITY AI################2.0###############################################

# image_label=customtkinter.CTkLabel(root, text="", image=stability, justify=LEFT)
# image_label.grid(column=85, row=0, columnspan=50)

###
label_model = customtkinter.CTkLabel(root, text='image generation model: ', justify=LEFT)
label_model.grid(column=85, row=5)

model_combo = customtkinter.CTkComboBox(root, values=models, width=180, height=27)
model_combo.grid(column=90, row=5)

label_width = customtkinter.CTkLabel(root, text=' image size: ', justify=LEFT)
label_width.grid(column=100, row=5)

width_entry = customtkinter.CTkComboBox(root, values=sizes, width=180, height=27)
width_entry.grid(column=105, row=5)

label_sampler = customtkinter.CTkLabel(root, text='sampler: ', justify=LEFT)
label_sampler.grid(column=85, row=7)

sampler_combo = customtkinter.CTkComboBox(root, values=samplers_list, width=180, height=27)
sampler_combo.grid(column=90, row=7)

save_btn = customtkinter.CTkButton(root, text="save", width=180, height=30, command=choise_dir,
                                   fg_color="blue")
save_btn.grid(column=105, row=7)

image_mega = customtkinter.CTkLabel(root, text="", image=main_image)
image_mega.grid(column=85, row=8, columnspan=100, rowspan=100)
root.mainloop()
