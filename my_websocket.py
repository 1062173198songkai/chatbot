import json
import websocket
from threading import Thread

class WebSocketClient:
    def __init__(self, uri, message_handler):
        self.uri = uri
        self.ws = None
        self.message_handler = message_handler

    def connect(self):
        self.ws = websocket.WebSocketApp(self.uri,
                                         on_message=self.on_message,
                                         on_error=self.on_error,
                                         on_close=self.on_close)
        self.thread = Thread(target=self.ws.run_forever)
        self.thread.start()
        print("Connected to the server")

    def send_message(self, message):
        # This function is called in a thread-safe manner from websocket-client's run_forever thread
        if self.ws:
            self.ws.send(message)
            print("Message sent:", message)
        else:
            print("Connection is not established.")

    def on_message(self, ws, message):
        print("Received raw message:", message)
        if self.message_handler:
            self.message_handler(message)

    def on_error(self, ws, error):
        print("WebSocket error:", error)

    def on_close(self, ws, close_status_code, close_msg):
        print("Connection closed.")

    def close(self):
        if self.ws:
            self.ws.close()

def message_handler(message):
    try:
        data = json.loads(message)
        print(f"Received message: {data}")
    except json.JSONDecodeError:
        print("Received non-JSON message.")

# Example usage
def main():
    uri = "ws://localhost:6789"
    client = WebSocketClient(uri, message_handler)
    client.connect()
    # Example sending a message, replace with your actual use case
    client.send_message("Hello, server!")

    # Since this is a demo, we're not implementing a full clean-up and exit strategy.
    # In a real application, ensure you gracefully close the connection and join the thread.

if __name__ == "__main__":
    main()
